function triggerOnEdit(e)
{
  showMessageOnUpdate(e);
}
 
function showMessageOnUpdate(e)
{
  var range = e.range;
  var row = range.getRow();
  var column = range.getColumn();
  var checkId = SpreadsheetApp.getActiveSheet().getRange(row,5).getValue();
  if (column <= 3 && row > 1 && checkId.trim() != "")
  {
    SpreadsheetApp.getActiveSheet().getRange(row,4).setValue('edit');
    //var ui = SpreadsheetApp.getUi();
    //var response = ui.alert("Event from row " + checkId + " has been modified"," Update clendar now ? ", ui.ButtonSet.YES_NO);
    //if (response = ui.Button.YES){
      addToRoaster();
   // }
   // else if(response = ui.Button.NO)
   // {
   //   return;
   // }
  }
}

function addToRoaster() 
{
  
  var sheet = SpreadsheetApp.getActiveSheet();
  var calendar = CalendarApp.getCalendarById('*******theCalID*******');
  var startRow = 2;  
  var numRows = sheet.getLastRow();
  var numColumns = sheet.getLastColumn();
  var dataRange = sheet.getRange(startRow, 1, numRows-1, numColumns);
  var data = dataRange.getValues();
  var complete = "ok";
 
  for (var i = 0; i < data.length; ++i) {
    var row = data[i];
    var name = row[2]; 
    var startTime =row[0];  
    var stopTime =row[1]; 
    var eventState = row[3]; 
    var eventId = row[4];
    
    if (eventState != complete) 
    {
      if (eventState == "edit")
      {
        var thisEvent = calendar.getEventSeriesById(eventId);
        thisEvent.deleteEventSeries();
        var id = calendar.createEvent(name, startTime, stopTime).getId();
        var thisId = dataRange.getCell(i+1,numColumns);
        thisId.setValue(id);
        var thisState = dataRange.getCell(i+1,numColumns-1);
        thisState.setValue(complete);
      }
      else
      {
        var id = calendar.createEvent(name, startTime, stopTime).getId();
        var thisId = dataRange.getCell(i+1,numColumns);
        thisId.setValue(id);
        var thisState = dataRange.getCell(i+1,numColumns-1);
        thisState.setValue(complete);
      }
    }
  }
}